from redbaron import RedBaron
import click

@click.command()
@click.option('--filename', type=str)
@click.option('--linenumber', type=int)
def main(filename, linenumber):
    x = linenumber
    with open(filename, 'r') as source_code:
        red = RedBaron(source_code.read())

    clss = [cls for cls in red if cls.type == 'class']

    print(red)


if __name__ == '__main__':
    main()
