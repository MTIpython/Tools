import click
import numpy as np

def K_A_builder(location):
    K_A = np.array([[1., 1.1, 1.25, 1.6],
                    [1.25, 1.35, 1.5, 1.75],
                    [1.5, 1.6, 1.75, 2.0],
                    [1.75, 18.5, 2.0, 2.25]])
    np.save(location + '/application_factor', K_A)


@click.command()
@click.option('--build', type=click.Choice(['all', 'data']), default='all')
@click.option('--location', type=str, default='.')
def main(build, location):
    if build == 'data' or build == 'all':
        K_A_builder(location + '/data')


if __name__ == '__main__':
    main()
