from MTIpython.units import u
from numpy import array
from MTIpython.fluid.pump import Pump

qh_lut = [array([0.600, 0.800, 1.000, 1.200, 1.400, 1.600, 1.800, 2.000, 2.200, 2.400, 2.600, 2.800, 3.000,
                 3.200, 3.400, 3.600, 3.800, 4.000, 4.200, 4.400, 4.600, 4.800,
                 5.000]) * u.m ** 3 / u.s ,
          array(
              [329.3, 322.6, 315.7, 308.7, 301.6, 294.3, 286.9, 279.4, 271.7, 263.9, 256.0, 247.9, 239.7, 231.3, 222.8,
               214.2, 205.4, 196.5, 187.5, 178.3, 169.0, 159.5, 149.9,
               ]) * u.m]

cs3b = Pump(name='cs3b', d=[0.850 * u.m, 0.850 * u.m], n=3.467 * u.Hz, Q_h=qh_lut)

cs3b.save(r'../../Resources/Components/Pumps/CentrifugalPumps/cs3b.cmp')
