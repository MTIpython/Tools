import click
import numpy as np

from MTIpython.core.binder import Binder, Function
from MTIpython.core.functions import LUT
from MTIpython.core.logging import setup_logging
from MTIpython.material import *
from MTIpython.material.granular import Configuration, GeldartGrouping, ParticleSizeDistribution
from MTIpython.material.principal.core import *
from MTIpython.material.principal.psychrometrics import MTICoolpropHA
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u

setup_logging()


# Fluid builder
def base_liquids(location):
    for sal in range(1, 13):
        seawater = Liquid(name=r'Sea Water {}%'.format(sal),
                          _cp_name='INCOMP::MITSW[0.{:02}]'.format(sal),
                          T=Binder(value=20. * u.degC, bounds=(0., inf) * u.K, unit=u.degC,
                                   func1=Function(MTICoolprop.temperature, material='_cp_name',
                                                  p='p', rho='rho')),
                          rho=Binder(bounds=(940., 1100.), unit=u.kg / u.m ** 3,
                                     func1=Function(density, v='v'),
                                     func2=Function(density, gamma='gamma'),
                                     func3=Function(density, V='V', m='m'),
                                     func4=Function(MTICoolprop.density, material='_cp_name', T='T',
                                                    p='p')),
                          p=Binder(value=1. * u.bar, bounds=(0., inf), unit=u.Pa,
                                   func1=Function(MTICoolprop.pressure, T='T', rho='rho')),
                          mu=Binder(bounds=(0., inf), unit=u.Pa * u.s,
                                    func1=Function(MTICoolprop.viscosity, material='_cp_name',
                                                   T='T', p='p'),
                                    func2=Function(dynamic_viscosity, rho='rho', nu='nu')),
                          k=Binder(bounds=(0., inf), unit=u.W / (u.m * u.K),
                                   func1=Function(MTICoolprop.conductivity, material='_cp_name',
                                                  T='T', p='p')),
                          c_p=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                                     func1=Function(MTICoolprop.heat_capacity, T='T', p='p',
                                                    material='_cp_name')))
        seawater.save(location + r'/seawater_{:02}.mtimat'.format(sal))

    water = Liquid(name=r'Water', _cp_name='Water',
                   T=Binder(value=20. * u.degC, bounds=(0., inf) * u.K, unit=u.degC,
                            func1=Function(MTICoolprop.temperature, material='_cp_name', p='p',
                                           rho='rho')),
                   rho=Binder(bounds=(940., 1100.), unit=u.kg / u.m ** 3,
                              func1=Function(density, v='v'),
                              func2=Function(density, gamma='gamma'),
                              func3=Function(density, V='V', m='m'),
                              func4=Function(MTICoolprop.density, material='_cp_name', T='T',
                                             p='p')),
                   p=Binder(value=1. * u.bar, bounds=(0., inf), unit=u.Pa,
                            func1=Function(MTICoolprop.pressure, T='T', rho='rho')),
                   mu=Binder(bounds=(0., inf), unit=u.Pa * u.s,
                             func1=Function(MTICoolprop.viscosity, material='_cp_name', T='T',
                                            p='p'),
                             func2=Function(dynamic_viscosity, rho='rho', nu='nu')),
                   k=Binder(bounds=(0., inf), unit=u.W / (u.m * u.K),
                            func1=Function(MTICoolprop.conductivity, material='_cp_name', T='T',
                                           p='p')),
                   c_p=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                              func1=Function(MTICoolprop.heat_capacity, T='T', p='p',
                                             material='_cp_name')))
    water.save(location + r'/water.mtimat')

    waterglas = Liquid(name='Water Glas', CAS='1344-09-8', T=20. * u.degC, rho=1300. * u.kg / u.m ** 3)
    waterglas.save(location + r'waterglas.mtimat')

    # Sources:
    # Japan Soda Industry Association, “Safe handling of caustic soda,” Nov. 2006.
    # OxyChem, “Oxychem Caustic Soda Handbook,” Mar. 2018.
    # DOW, “Dow Caustic Soda Solution Handbook.”
    caustic_soda_50 = Liquid(name='Caustic Soda 50%', CAS='1310-73-2', p=1. * u.bar, m=1. * u.kg,
                             T=Binder(value=20. * u.degC, bounds=(15., 110.), unit=u.degC),
                             rho=Binder(unit=u.kg / u.m ** 3,
                                        func1=Function(LUT(T=[15, 18, 20, 30, 40, 50, 60, 70, 80, 90, 100] * u.degC,
                                                           rho=[1.5290, 1.5268, 1.5253, 1.5181, 1.5109, 1.5038, 1.4967,
                                                                1.4897, 1.4827, 1.4759, 1.4690] * u.g / u.cm ** 3),
                                                       T='T')),
                             mu=Binder(unit=u.mPa * u.s,
                                       func1=Function(LUT(T=[15., 30., 50., 70., 90., 110.] * u.degC,
                                                          mu=[90., 38., 15., 7.5, 4., 2.2] * u.mPa * u.s), T='T')),
                             c_p=Binder(unit=u.kJ / (u.kg * u.K),
                                        func1=Function(LUT(T=[30., 50., 100., 140.] * u.degC,
                                                           c_p=[3242.6, 3221.68, 3200.76, 3179.84] * u.kJ / (
                                                                   u.kg * u.K)), T='T')),
                             k=Binder(unit=u.W / (u.K * u.m),
                                      func1=Function(LUT(T=[21.1, 32.2, 37.8, 54.4, 76.7, 87.8] * u.degC,
                                                         c_p=[0.65075623, 0.66667899, 0.68225561, 0.69748607,
                                                              0.70960121, 0.71998562] * u.W / (u.K * u.m)), T='T'))
                             )

    caustic_soda_50.save(location + r'/caustic_soda_50.mtimat')


# Metal builder
def base_metals(location):
    rho = 7800. * u.kg / u.m ** 3

    # Source: roloff/matek
    s235jr = Metal(name='S235JR', rho=rho, T=20. * u.degC,
                   E=210. * u.GPa, G=81. * u.GPa, R_mN=360. * u.MPa, R_eN=235. * u.MPa,
                   sigma_tdWN=140. * u.MPa,
                   sigma_bWN=180. * u.MPa, tau_tWN=105. * u.MPa, rel_cost=1.0, epsilon=4.6e-3)
    s235jr.save(location + r'/s235jr.mtimat')

    # Source: roloff/matek
    s275jr = Metal(name='S275JR', rho=rho, T=20. * u.degC,
                   E=210. * u.GPa, G=81. * u.GPa, R_mN=430. * u.MPa, R_eN=275. * u.MPa,
                   sigma_tdWN=170. * u.MPa,
                   sigma_bWN=215. * u.MPa, tau_tWN=125. * u.MPa, rel_cost=1.05, epsilon=4.6e-3)
    s275jr.save(location + r'/s275jr.mtimat')

    # Source: roloff/matek
    s355jr = Metal(name='S355JR', rho=rho, T=20. * u.degC,
                   E=210. * u.GPa, G=81. * u.GPa, R_mN=510. * u.MPa, R_eN=355. * u.MPa,
                   sigma_tdWN=205. * u.MPa,
                   sigma_bWN=255. * u.MPa, tau_tWN=150. * u.MPa, rel_cost=2., epsilon=4.6e-3)
    s355jr.save(location + r'/s355jr.mtimat')

    # Source: roloff/matek
    C45E = Metal(name='C45E', rho=rho, T=20. * u.degC,
                 E=210. * u.GPa, G=81. * u.GPa, R_mN=700. * u.MPa, R_eN=490. * u.MPa,
                 sigma_tdWN=280. * u.MPa,
                 sigma_bWN=350. * u.MPa, tau_tWN=210. * u.MPa, rel_cost=1.7, epsilon=4.6e-3)
    C45E.save(location + r'/C45E.mtimat')

    # Source: roloff/matek & http://matweb.com/search/DataSheet.aspx?MatGUID=dd78e2e4436f497ebb862ea737b8139d
    en14404 = Metal(name='1.4404',
                    rho=rho,
                    T=20. * u.degC,
                    R_mN=Binder(unit=u.MPa,
                                func1=Function(LUT(T=[-196., -80., 20., 100., 200., 300.] * u.degC,
                                                   R_mN=[1200., 840., 570., 430., 390., 380.] * u.MPa), T='T')),
                    R_eN=Binder(unit=u.MPa,
                                func1=Function(LUT(T=[-196., -80., 20., 100., 200., 300., 400., 500.] * u.degC,
                                                   R_eN=[450., 355., 320., 211., 177., 156., 144., 139.] * u.MPa),
                                               T='T')),
                    E=Binder(unit=u.GPa,
                             func1=Function(LUT(T=[20., 100., 200., 300., 400., 500.] * u.degC,
                                                E=[200., 194., 186., 179., 172., 165.] * u.GPa), T='T')),
                    k=Binder(unit=u.W / (u.m * u.K),
                             func1=Function(LUT(T=[20., 400.] * u.degC,
                                                k=[15., 20.] * u.W / (u.m * u.K)), T='T')),
                    c_p=0.5 * u.J / (u.g * u.K),
                    rel_cost=5.8,
                    epsilon=4.6e-3,
                    sigma_tdWN=220. * u.MPa,
                    sigma_bWN=260. * u.MPa,
                    tau_tWN=150. * u.MPa,
                    G=77. * u.GPa)
    en14404.save(location + r'/en-14404.mtimat')


# Gas builder
def base_gasses(location):
    air_dry = Gas(name='Air dry', _cp_name='air',
                  T=Binder(value=20. * u.degC, bounds=(0., inf) * u.K, unit=u.degC,
                           func1=Function(MTICoolprop.temperature, material='_cp_name', p='p',
                                          rho='rho')),
                  rho=Binder(bounds=(0., inf), unit=u.kg / u.m ** 3,
                             func1=Function(density, v='v'),
                             func2=Function(density, gamma='gamma'),
                             func3=Function(density, V='V', m='m'),
                             func4=Function(MTICoolprop.density, material='_cp_name', T='T',
                                            p='p')),
                  p=Binder(value=1. * u.bar, bounds=(0., inf), unit=u.Pa,
                           func1=Function(MTICoolprop.pressure, T='T', rho='rho')),
                  mu=Binder(bounds=(0., inf), unit=u.Pa * u.s,
                            func1=Function(MTICoolprop.viscosity, material='_cp_name', T='T',
                                           p='p')),
                  nu=Binder(bounds=(inf, -inf), unit=u.m ** 2 / u.s,
                            func1=Function(kinematic_viscosity, rho='rho', mu='mu')),
                  k=Binder(bounds=(0., inf), unit=u.W / (u.m * u.K),
                           func1=Function(MTICoolprop.conductivity, material='_cp_name', T='T',
                                          p='p')),
                  c_p=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                             func1=Function(MTICoolprop.heat_capacity, T='T', p='p',
                                            material='_cp_name')),
                  Z=Binder(bounds=(-inf, inf), unit=u.dimensionless,
                           func1=Function(MTICoolprop.compressibility_factor, T='T', p='p',
                                          material='_cp_name')))
    air_dry.save(location + r'/air_dry.mtimat')

    air_humid = HumidAir(name='Air humid',
                         T=Binder(value=20. * u.degC, bounds=(-273.15, inf) * u.degC, unit=u.degC,
                                  func1=Function(MTICoolpropHA.temperature, p='p', v='v', RH='RH')),
                         rho=Binder(bounds=(0., inf), unit=u.kg / u.m ** 3,
                                    func1=Function(density, v='v'),
                                    func2=Function(density, gamma='gamma'),
                                    func3=Function(density, V='V', m='m')),
                         v=Binder(bounds=(0., inf), unit=u.m ** 3 / u.kg,
                                  func1=Function(MTICoolpropHA.specific_volume, p='p', T='T',
                                                 RH='RH'),
                                  func2=Function(specific_volume, rho='rho'),
                                  func3=Function(specific_volume, m='m', V='V')),
                         p=Binder(value=1. * u.bar, bounds=(0., inf), unit=u.Pa),
                         mu=Binder(bounds=(0., inf), unit=u.Pa * u.s,
                                   func1=Function(MTICoolpropHA.dynamic_viscosity, p='p', T='T',
                                                  RH='RH'),
                                   func2=Function(dynamic_viscosity, rho='rho', nu='nu')),
                         k=Binder(bounds=(0., inf), unit=u.W / (u.m * u.K),
                                  func1=Function(MTICoolpropHA.conductivity, p='p', T='T',
                                                 RH='RH')),
                         c_p=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                                    func1=Function(MTICoolpropHA.specific_heat_const_pressure,
                                                   p='p', T='T', RH='RH')),
                         h=Binder(bounds=(-inf, inf), unit=u.J / u.kg,
                                  func1=Function(MTICoolpropHA.specific_enthalpy, p='p', T='T',
                                                 RH='RH')),
                         H=Binder(bounds=(-inf, inf), unit=u.J,
                                  func1=Function(enthalpy, h='h', m='m')),
                         s=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                                  func1=Function(MTICoolpropHA.specific_entropy, p='p', T='T',
                                                 RH='RH')),
                         RH=Binder(bounds=(0., 1.), unit=u.dimensionless,
                                   func1=Function(MTICoolpropHA.relative_humidity, p='p', T='T', W='W')),
                         W=Binder(value=0.1, bounds=(0., inf), unit=u.g / u.kg,
                                  func1=Function(MTICoolpropHA.water_content, p='p', T='T', RH='RH')),
                         T_wb=Binder(bounds=(-inf, inf), unit=u.degC,
                                     func1=Function(MTICoolpropHA.wetbulb_temperature, p='p', T='T',
                                                    RH='RH')),
                         Pr=Binder(bounds=(-inf, inf), unit=u.dimensionless,
                                   func1=Function(MTICoolprop.prandtl, p='p', T='T', material='_propsi_name')),
                         m=1. * u.kg)
    air_humid.save(location + r'/air_humid.mtimat')

    ammonia = Gas(name='Ammonia', _cp_name='Ammonia',
                  T=Binder(value=20. * u.degC, bounds=(0., inf) * u.K, unit=u.degC,
                           func1=Function(MTICoolprop.temperature, material='_cp_name', p='p',
                                          rho='rho')),
                  rho=Binder(bounds=(0., inf), unit=u.kg / u.m ** 3,
                             func1=Function(density, v='v'),
                             func2=Function(density, gamma='gamma'),
                             func3=Function(density, V='V', m='m'),
                             func4=Function(MTICoolprop.density, material='_cp_name', T='T',
                                            p='p')),
                  p=Binder(value=1. * u.bar, bounds=(0., inf), unit=u.Pa,
                           func1=Function(MTICoolprop.pressure, T='T', rho='rho')),
                  mu=Binder(bounds=(0., inf), unit=u.Pa * u.s,
                            func1=Function(MTICoolprop.viscosity, material='_cp_name', T='T',
                                           p='p'),
                            func2=Function(dynamic_viscosity, rho='rho', nu='nu')),
                  k=Binder(bounds=(0., inf), unit=u.W / (u.m * u.K),
                           func1=Function(MTICoolprop.conductivity, material='_cp_name', T='T',
                                          p='p')),
                  c_p=Binder(bounds=(-inf, inf), unit=u.J / (u.kg * u.K),
                             func1=Function(MTICoolprop.heat_capacity, T='T', p='p',
                                            material='_cp_name')),
                  Pr=Binder(bounds=(-inf, inf), unit=u.dimensionless,
                            func1=Function(MTICoolprop.prandtl, p='p', T='T',
                                           material='_cp_name')),
                  Z=Binder(bounds=(-inf, inf), unit=u.dimensionless,
                           func1=Function(MTICoolprop.compressibility_factor, T='T', p='p',
                                          material='_cp_name')),
                  m=1. * u.kg)
    ammonia.save(location + r'/ammonia.mtimat')


# Solid builder
def base_solids(location):
    amoras = Solid(name='Amoras', rho=2000. * u.kg / u.m ** 3, T=20. * u.degC, category=Category.UNDEFINED)
    amoras.save(location + r'/amoras.mtimat')
    bfs = Solid(name='Blast Furnance Slag', rho=2800. * u.kg / u.m ** 3, T=20. * u.degC, category=Category.UNDEFINED)
    bfs.save(location + r'/BFS.mtimat')
    superwool_128 = Solid(name='Super Wool 128',
                          rho=128. * u.kg / u.m ** 3,
                          T=20. * u.degC,
                          category=Category.UNDEFINED,
                          k=Binder(unit=u.W / (u.m * u.K),
                                   func1=Function(LUT(T=[200., 400., 600., 800., 1000.] * u.degC,
                                                      k=[0.05, 0.08, 0.12, 0.18, 0.25] * u.W / (u.m * u.K)), T='T')))
    superwool_128.save(location + r'/superwool_128.mtimat')
    thermall_1430 = Solid(name='ThermAll 1430',
                          rho=128. * u.kg / u.m ** 3,
                          T=20. * u.degC,
                          category=Category.UNDEFINED,
                          k=Binder(unit=u.W / (u.m * u.K),
                                   func1=Function(LUT(T=[800., 1000.] * u.degC,
                                                      k=[0.16, 0.2] * u.W / (u.m * u.K)), T='T')))
    thermall_1430.save(location + r'/thermall_1430.mtimat')
    thermall_1260 = Solid(name='ThermAll 1260',
                          rho=128. * u.kg / u.m ** 3,
                          T=20. * u.degC,
                          category=Category.UNDEFINED,
                          k=Binder(unit=u.W / (u.m * u.K),
                                   func1=Function(LUT(T=[400., 800.] * u.degC,
                                                      k=[0.09, 0.16] * u.W / (u.m * u.K)), T='T')))
    thermall_1260.save(location + r'/thermall_1260.mtimat')


# Plastic builder
def base_plastics(location):
    # Source: Roloff/Matek and https://www.makeitfrom.com/material-properties/Unplasticized-Rigid-Polyvinyl-Chloride-uPVC-PVC-U
    pvcu = Plastic(name='PVC-U', rho=1.38 * u.kg / u.m ** 3, sigma_M=50. * u.MPa, k=0.16 * u.W / (u.m * u.K),
                   T_use=[-5., 65.] * u.degC, epsilon_m=0.04, E=3. * u.GPa, CAS='polyvinylchloride hard',
                   epsilon=0.0015 * u.mm, c_p=880. * u.J / (u.kg * u.K))
    pvcu.save(location + r'/PVC-U.mtimat')


# Granulate builder
def base_granulates(location):
    pass


# Powder builder
def base_powders(location):
    amoras = material_factory['amoras']
    amoras_powder = Powder(name='Amoras_powder', rho=1600. * u.kg / u.m ** 3, T=20. * u.degC,
                           basematerial=amoras,
                           configuration=Configuration.RLP,
                           PSD=ParticleSizeDistribution(
                               fraction=np.array([0.1, 0.25, 0.5, 0.75, 0.9, 1.]) * u.dimensionless,
                               size=np.array([2.051, 3.431, 7.978, 18.8, 33.8, 80.]) * u.um),
                           geldart_group=GeldartGrouping.A)
    amoras_powder.save(location + r'/amoras_powder.mtimat')

    bfs = material_factory['BFS']

    def packing_rho(configuration: Configuration):
        if configuration == Configuration.RLP:
            return 900. * u.kg / u.m ** 3
        else:
            return 1200. * u.kg / u.m ** 3

    gbfs = Powder(name='Granulated Blast Furnance Slag', CAS='65996-69-2', T=20. * u.degC,
                  rho=Binder(bounds=(0., inf), unit=u.kg / u.m ** 3,
                             func1=Function(packing_rho, configuration='configuration')),
                  basematerial=bfs,
                  configuration=Configuration.RLP,
                  PSD=ParticleSizeDistribution(fraction=np.array([0.1, 0.25, 0.5, 0.75, 0.9, 1.]) * u.dimensionless,
                                               size=np.array([3.023, 6.8, 13.99, 23.75, 35.14, 40.]) * u.um))
    gbfs.save(location + r'/GBFS.mtimat')


@click.command()
@click.option('--build',
              type=click.Choice(
                  ['all', 'liquids', 'metals', 'plastics', 'gasses', 'granulars', 'solids',
                   'powders']),
              default='all')
@click.option('--location', type=str, default='.')
def main(build, location):
    if build == 'gasses' or build == 'all':
        base_gasses(location + '/gasses')
    if build == 'liquids' or build == 'all':
        base_liquids(location + '/liquids')
    if build == 'metals' or build == 'all':
        base_metals(location + '/metals')
    if build == 'solids' or build == 'all':
        base_solids(location + '/solids')
    if build == 'plastics' or build == 'all':
        base_plastics(location + '/plastics')
    if build == 'granulars' or build == 'all':
        base_granulates(location + '/granulars')
    if build == 'powders' or build == 'all':
        base_powders(location + '/powders')


if __name__ == '__main__':
    main()
