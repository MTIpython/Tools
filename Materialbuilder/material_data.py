import click
import yaml


# principal lut builder
def core_lut(location):
    FKM_col = ['CARBURIZED', 'WROUGHT', 'STAINLESS', 'MISC', 'GS', 'GJS', 'GJM', 'GJL']
    FKM_guidelines = {'f_sigma_t': {FKM_col[0]: 1.,
                                    FKM_col[1]: 1.,
                                    FKM_col[2]: 1.,
                                    FKM_col[3]: 1.,
                                    FKM_col[4]: 1.,
                                    FKM_col[5]: 1.,
                                    FKM_col[6]: 1.,
                                    FKM_col[7]: 1.,
                                    },
                      'f_sigma_c': {FKM_col[0]: 1.,
                                    FKM_col[1]: 1.,
                                    FKM_col[2]: 1.,
                                    FKM_col[3]: 1.,
                                    FKM_col[4]: 1.,
                                    FKM_col[5]: 1.3,
                                    FKM_col[6]: 1.5,
                                    FKM_col[7]: 2.5,
                                    },
                      'f_tau':     {FKM_col[0]: 0.58,
                                    FKM_col[1]: 0.58,
                                    FKM_col[2]: 0.58,
                                    FKM_col[3]: 0.58,
                                    FKM_col[4]: 0.58,
                                    FKM_col[5]: 0.65,
                                    FKM_col[6]: 0.75,
                                    FKM_col[7]: 0.85,
                                    },
                      'f_sigma_w': {FKM_col[0]: 0.4,
                                    FKM_col[1]: 0.4,
                                    FKM_col[2]: 0.4,
                                    FKM_col[3]: 0.45,
                                    FKM_col[4]: 0.34,
                                    FKM_col[5]: 0.34,
                                    FKM_col[6]: 0.3,
                                    FKM_col[7]: 0.3,
                                    },
                      'f_tau_w':   {FKM_col[0]: 0.58,
                                    FKM_col[1]: 0.58,
                                    FKM_col[2]: 0.58,
                                    FKM_col[3]: 0.58,
                                    FKM_col[4]: 0.58,
                                    FKM_col[5]: 0.65,
                                    FKM_col[6]: 0.75,
                                    FKM_col[7]: 0.85,
                                    }
                      }
    with open(location + '/strength_conversion_factor.mtidat', 'w') as f:
        header = [r'# MTIpython material resource file.', '\n',
                  r'# Conversion factor for tension/compression, shear and fatigue strengths according FKM-guidelines',
                  '\n',
                  r'# Muhs et al. (2011). Roloff/Matek machine-onderdelen: tabellenboek (4th ed.). SDU Uitgevers.', '\n']
        f.writelines(header)
        yaml.dump(FKM_guidelines, f, default_flow_style=False)


@click.command()
@click.option('--build', type=click.Choice(['all', 'principal']), default='all')
@click.option('--location', type=str, default='.')
def main(build, location):
    if build == 'principal' or build == 'all':
        core_lut(location)


if __name__ == '__main__':
    main()
