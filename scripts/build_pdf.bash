#!/bin/bash
# build script for pdflatex ignoring fault code and generating one when pdf is not made
# Build the pdf 2 times in order to make the reference work
cd Documentation/_build/latex
pdflatex -interaction=nonstopmode MTIpython.tex || :
pdflatex -interaction=nonstopmode MTIpython.tex || :
if [ ! -f  MTIpython.pdf ]; then
    echo "File not found!"
    exit 1
fi
cp MTIpython.pdf ../../../
