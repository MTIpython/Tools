import os
import shutil
import subprocess

import click
from pylint.lint import Run
from pylint.reporters.json import JSONReporter
from rstcloth.rstcloth import RstCloth
from rstcloth.table import RstTable, TableData

from MTIpython.core.logging import setup_logging
import logging


def get_badge(res):
    if res < 5.:
        color = 'red'
    elif res < 7.:
        color = 'yellow'
    else:
        color = 'brightgreen'

    status = r"{neg}{res:.3}%2F10".format(res=res, neg='-' if res < 0. else '')
    return r"https://img.shields.io/badge/pylint-{status:}-{color:}.svg".format(status=status,
                                                                                color=color)


def git_commit_version():
    cversion = subprocess.check_output(["/usr/bin/git", "describe", "--tags", "HEAD"]).decode("utf-8")[:-1]
    logging.info("Git version is: {}".format(cversion))
    return cversion


def git_commit_hash():
    chash = subprocess.check_output(["/usr/bin/git", "rev-parse", "--verify", "HEAD"]).decode("utf-8")[:-1]
    logging.info("Git commit has is: {}".format(chash))
    return chash


def git_remote_url():
    return r"""http://mti-gitlab.ihc.eu/MTIpython/MTIpython/"""
    # return subprocess.check_output(["/usr/bin/git", "remote", "get-url", "origin"]).decode(
    # "utf-8")[
    #        :-5]


def get_line_url(remote_url, commit, filename, line_number):
    return get_path_url(remote_url, commit, filename) + "#L{}".format(line_number)


def get_path_url(remote_url, commit, filename):
    return "{}blob/{}/{}".format(remote_url, commit, filename)


def dl_badge(folder, url):
    logging.info("Downloading badge from {}".format(url))
    subprocess.call(["/usr/bin/wget", "-O", "{}/pylint_badge.svg".format(folder), url])


def clean_folder(folder):
    logging.info('Cleaning old files')
    for f in os.listdir(folder):
        f_path = os.path.join(folder, f)
        if os.path.isfile(f_path):
            os.unlink(f_path)


def init_folder(folder):
    logging.info('initalizing folder, with Sphinx setup files')
    dir_path = os.path.dirname(os.path.realpath(__file__))
    shutil.copy('{}/resources/Makefile'.format(dir_path), folder)
    shutil.copy('{}/resources/conf.py'.format(dir_path), folder)


@click.command()
@click.option('--folder', type=str)
def main(folder):
    setup_logging()
    logging.info('Starting pylin2rst')
    logging.info('Current folder is: {}'.format(os.getcwd()))
    os.chdir(os.path.dirname(os.path.realpath(__file__)) + "/../../")
    logging.info('Changed current folder to {}'.format(os.getcwd()))
    if not os.path.exists(folder):
        logging.info('Creatign pylint folder')
        os.makedirs(folder)
    clean_folder(folder)
    init_folder(folder)
    origin_url = git_remote_url()
    commit_hash = git_commit_hash()
    reporter = JSONReporter()
    results = Run(['MTIpython'], reporter=reporter, exit=False)

    dl_badge(folder, get_badge(results.linter.stats['global_note']))
    rst = RstCloth()
    rst.title("PyLint report for MTIpython version: {}".format(git_commit_version()))
    rst.newline()
    rst.directive('image', 'pylint_badge.svg')
    rst.newline()
    rst.h1("Pylint messages")
    rst.directive('toctree')
    modules = sorted(list(set([".".join(module.split('.')) for module in
                               list(results.linter.stats['by_module'].keys())])))
    rst.newline()
    for m in modules:
        if len([m_msg for m_msg in reporter.messages if m_msg['module'] == m]) > 0:
            rst.content(m, indent=3)
    rst.write('{}/pylint.rst'.format(folder))
    rst.print_content()
    del rst

    for m in modules:
        rst_mod = RstCloth()
        rst_mod.h1(m)
        rst_mod.newline()
        mod_msgs = [m_msg for m_msg in reporter.messages if m_msg['module'] == m]
        for msg in mod_msgs:
            tbl = TableData(num_columns=2)
            for key, value in msg.items():
                if key == 'line':
                    value = '`{} <{}>`_'.format(value,
                                                get_line_url(origin_url, commit_hash, msg['path'],
                                                             value))
                if key == 'path':
                    value = '`{} <{}>`_'.format(value,
                                                get_path_url(origin_url, commit_hash, value))
                if key == 'message':
                    value = "``{}``".format(value)
                if key != 'module':
                    tbl.add_row([key, "{}".format(value)])
            tbl.finalize()
            rst_tbl = RstTable(tbl)
            rst_mod.content(rst_tbl.output)
            rst_mod.newline()
        if len(mod_msgs) > 0:
            rst_mod.write('{}/{}.rst'.format(folder, m))
            rst_mod.print_content()


if __name__ == '__main__':
    main()
